import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

export default function Form({ addHandler }) {
    const [text, setValue] = useState('');
    const onChange = (text) => {
        setValue(text);

    };
    return (

        <View style={styles.main} >
            <TextInput style={styles.imput} onChangeText={onChange} placeholder='План..' />
            <Button color="green" onPress={() => addHandler(text)} title='Добавить план' />
        </View>
    );
}

const styles = StyleSheet.create({

    imput: {
        borderBottomWidth: 1,
        borderColor: '#000',
        padding: 10,
        marginVertical: 30,
        marginHorizontal: "20%",
        width: "60%"
    }
});