import React, { useState } from 'react';
import { StyleSheet, View, FlatList, Text } from 'react-native';
import Header from "./components/Header";
import List from "./components/List";
import Form from "./components/Form";
export default function App() {
  const [listOfItems, setListOfItems] = useState([
    { text: 'Купить молоко', kye: "1" },
    { text: 'Купить печенье', key: "2" },
    { text: 'Купить авокадо', key: "3" },
    { text: 'Купить булгур', key: "4" },
  ])

  const addHandler = (text) => {
    setListOfItems((list) => {// list - получаем параметры текущего списка
      return [
        {
          text: text, key: Math.random().toString(36).substring(7)//случайный ключ
        }, ...list//возвращаем нынешний список  
      ]
    })
  }

  const deleteHandler=(key)=> {
    setListOfItems((list)=>{
      return list.filter(listOfItems=> listOfItems.key !=key)
    });
  }
  return (

    <View >
      <Header />
      <Form addHandler={addHandler} />
      <View>
        <FlatList data={listOfItems} renderItem={({ item }) => (
          <List el={item} deleteHandler={deleteHandler} />
        )} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({

});
